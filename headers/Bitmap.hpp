#ifndef BITMAP_HPP_
#define BITMAP_HPP_
#include <cstdint>
#include <iostream>
#include "interfaces/IBitmap.hpp"
namespace kk {

class Bitmap : public IBitmap {
public:
	Bitmap(uint16_t width, uint16_t height);
	uint8_t* GetPixels() const;
	uint8_t** GetImage() const;
	uint16_t GetHeight() const;
	uint16_t GetWidth() const;
	uint8_t GetPixel(int x, int y) const;
	uint8_t SetPixel(int x, int y, uint8_t color);
	void Load(std::string filename);
	void Save(std::string filename);
	void Blit(IBitmap * other, int x, int y);
	virtual ~Bitmap();
private:
	uint8_t ** columns;
	uint8_t * pixels;
	uint16_t width;
	uint16_t height;
};
}
#endif /* BITMAP_HPP_ */
