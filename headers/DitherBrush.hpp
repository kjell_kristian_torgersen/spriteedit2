/*
 * DitherBrush.hpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#ifndef DITHERBRUSH_HPP_
#define DITHERBRUSH_HPP_

#include "interfaces/IBrush.hpp"
#include "headers/Bitmap.hpp"

namespace kk {

class DitherBrush: public IBrush {
public:
	DitherBrush(IBitmap * bitmap);
	int Draw(int x, int y, int color);
	virtual ~DitherBrush();
private:
	IBitmap * bitmap;
};

} /* namespace kk */

#endif /* DITHERBRUSH_HPP_ */
