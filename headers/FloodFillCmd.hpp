#ifndef FLOODFILLCMD_HPP_
#define FLOODFILLCMD_HPP_

#include "interfaces/ICommand.hpp"
#include "headers/Model.hpp"

namespace kk {

class FloodFillCmd: public ICommand {
public:
	FloodFillCmd(IModel * model, int x, int y, int newColor);
	void Execute(void);
	void Undo(void);
	virtual ~FloodFillCmd();
private:
	void FloodFill(int x, int y);
	IModel * model;
	Bitmap undo;
	Bitmap plan;
	const int x;
	const int y;
	const int newColor;
	uint8_t ** image;
	int oldColor;

};

} /* namespace kk */

#endif /* FLOODFILLCMD_HPP_ */
