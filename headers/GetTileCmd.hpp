/*
 * GetTileCmd.hpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#ifndef GETTILECMD_HPP_
#define GETTILECMD_HPP_

#include "interfaces/ITileCmd.hpp"
#include "headers/Model.hpp"

namespace kk {

class GetTileCmd: public ITileCmd {
public:
	GetTileCmd(IModel * model);
	void SetSelector(int x, int y);
	void Execute(void);
	void Undo(void);
	virtual ~GetTileCmd();
private:
	IModel * model;
	Bitmap undoSprite;
	int selectorX;
	int selectorY;
};

} /* namespace kk */

#endif /* GETTILECMD_HPP_ */
