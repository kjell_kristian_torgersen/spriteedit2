#ifndef MODEL_HPP_
#define MODEL_HPP_

#include "Bitmap.hpp"
#include <stack>
#include "interfaces/ICommand.hpp"
#include "interfaces/IBrush.hpp"
#include "interfaces/IModel.hpp"

namespace kk {
class Model : public IModel {
public:
	Model();
	virtual ~Model();
	IBitmap * GetSprite();
	IBitmap * GetTiles();
	void SetImage(IBitmap * image);
	void SetTiles(IBitmap * tiles);
	void ExecuteCommand(ICommand * command);
	void Undo(void);
	void ChangeBrush(Brushes newBrush);
	int Draw(int x, int y, int color);
private:
	std::stack<ICommand*> commands;
	IBitmap * image;
	IBitmap * tiles;
	IBrush * brush;
};
}
#endif /* MODEL_HPP_ */
