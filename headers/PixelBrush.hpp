/*
 * PixelBrush.hpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#ifndef PIXELBRUSH_HPP_
#define PIXELBRUSH_HPP_

#include "interfaces/IBrush.hpp"
#include "interfaces/IBitmap.hpp"

namespace kk {

class PixelBrush: public IBrush {
public:
	PixelBrush(IBitmap * bitmap);
	int Draw(int x, int y, int color);
	virtual ~PixelBrush();
private:
	IBitmap * bitmap;
};

} /* namespace kk */

#endif /* PIXELBRUSH_HPP_ */
