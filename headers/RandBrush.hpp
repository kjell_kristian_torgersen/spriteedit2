/*
 * RandBrush.hpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#ifndef RANDBRUSH_HPP_
#define RANDBRUSH_HPP_

#include "headers/Bitmap.hpp"
#include "interfaces/IBrush.hpp"
#include <random>

namespace kk {

class RandBrush: public IBrush {
public:
	RandBrush(IBitmap * bitmap);
	int Draw(int x, int y, int color);
	virtual ~RandBrush();
private:
	IBitmap * bitmap;
	std::mt19937 rng;

};

} /* namespace kk */

#endif /* RANDBRUSH_HPP_ */
