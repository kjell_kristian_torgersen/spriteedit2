#ifndef SFMLDRAWSTATE_HPP_
#define SFMLDRAWSTATE_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "headers/Model.hpp"
#include "interfaces/ISFMLGUIState.hpp"
#include "headers/SFMLGUI.hpp"

namespace kk {

class SFMLDrawState: public ISFMLGUIState {
public:
	SFMLDrawState(SFMLGUI * gui, int gridSize);
	void Draw(void);
	bool HandleEvents(void);
	bool HandleKeyboard(sf::Event& event);
	virtual ~SFMLDrawState();
private:
	sf::RectangleShape rect[17];
	SFMLGUI * gui;
	bool drawing;
	int color[2];
	sf::RectangleShape sel[2];
	sf::Texture texture;
	int gridSize;
};

} /* namespace kk */

#endif /* SFMLDRAWSTATE_HPP_ */
