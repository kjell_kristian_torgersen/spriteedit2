#ifndef SFMLGUI_HPP_
#define SFMLGUI_HPP_

#include <stack>
#include <SFML/Graphics.hpp>

#include "headers/Model.hpp"
#include "interfaces/ISFMLGUIState.hpp"

namespace kk {

void UpdateTexture(sf::Texture* texture, IBitmap* bitmap);

class SFMLGUI {
public:
	SFMLGUI(kk::IModel * model);
	void Run();
	virtual ~SFMLGUI();
	kk::IModel* GetModel();
	sf::Color* GetPalette();
	sf::RenderWindow * GetWindow();
	void PushState(ISFMLGUIState * state);
private:
	kk::IModel * model;
	sf::RenderWindow window;
	sf::Color palette[17];
	std::stack<ISFMLGUIState *> states;
};
}
#endif /* SFMLGUI_HPP_ */
