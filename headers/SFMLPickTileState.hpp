/*
 * SFMLPickTileState.hpp
 *
 *  Created on: Oct 26, 2016
 *      Author: kjell
 */

#ifndef SFMLPICKTILESTATE_HPP_
#define SFMLPICKTILESTATE_HPP_

#include "headers/SFMLGUI.hpp"
#include "interfaces/ISFMLGUIState.hpp"
#include "interfaces/ICommand.hpp"
#include "interfaces/ITileCmd.hpp"
#include <SFML/Graphics.hpp>

namespace kk {

class SFMLPickTileState: public ISFMLGUIState {
public:
	SFMLPickTileState(SFMLGUI * gui, ITileCmd * tilePicked, sf::Color selectorColor);
	void Draw(void);
	bool HandleEvents(void);
	virtual ~SFMLPickTileState();
private:
	SFMLGUI * gui;
	ITileCmd * tilePicked;
	sf::Color selectorColor;
	sf::Sprite tiles;
	int selectorX;
	int selectorY;
	sf::Texture texture;
};

} /* namespace kk */

#endif /* SFMLPICKTILESTATE_HPP_ */
