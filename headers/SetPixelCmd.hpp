#ifndef SETPIXELCMD_HPP_
#define SETPIXELCMD_HPP_

#include "interfaces/ICommand.hpp"
#include "headers/Model.hpp"

namespace kk {

class SetPixelCmd: public ICommand {
public:
	SetPixelCmd(IModel * model, int x, int y, int newColor);
	void Execute(void);
	void Undo(void);
	virtual ~SetPixelCmd();
private:
	IModel * model;
	const int x;
	const int y;
	const int newColor;
	int oldColor;

};
} /* namespace kk */

#endif /* SETPIXELCMD_HPP_ */
