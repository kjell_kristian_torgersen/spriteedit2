/*
 * SetTileCmd.hpp
 *
 *  Created on: Oct 26, 2016
 *      Author: kjell
 */

#ifndef SETTILECMD_HPP_
#define SETTILECMD_HPP_

#include "interfaces/ITileCmd.hpp"
#include "headers/Model.hpp"
#include "headers/Bitmap.hpp"

namespace kk {

class SetTileCmd: public ITileCmd {
public:
	SetTileCmd(IModel * model);
	void SetSelector(int x, int y);
	void Execute(void);
	void Undo(void);
	virtual ~SetTileCmd();
private:
	IModel * model;
	Bitmap undoSprite;
	int selectorX;
	int selectorY;
};

} /* namespace kk */

#endif /* SETTILECMD_HPP_ */
