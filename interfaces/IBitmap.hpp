/*
 * IBitmap.hpp
 *
 *  Created on: Oct 28, 2016
 *      Author: kjell
 */

#ifndef IBITMAP_HPP_
#define IBITMAP_HPP_

#include <cstdint>

namespace kk {

class IBitmap {
public:
	/** \brief Get access to the raw pixel data */
	virtual uint8_t* GetPixels() const =0;
	/** \brief Get access to the image as something that behaves as a two dimensional array */
	virtual uint8_t** GetImage()const=0;
	/** \brief Get height of image */
	virtual uint16_t GetHeight()const=0;
	/** \brief Get width of image */
	virtual uint16_t GetWidth()const=0;
	/** \brief Get a pixel at position x,y */
	virtual uint8_t GetPixel(int x, int y)const=0;
	/** \brief Set a pixel at position x,y, return old pixel color */
	virtual uint8_t SetPixel(int x, int y, uint8_t color)=0;
	/** \brief Copy another image into this image at position x,y */
	virtual void Blit(IBitmap * other, int x, int y)=0;
	virtual ~IBitmap(){}
};

} /* namespace kk */

#endif /* IBITMAP_HPP_ */
