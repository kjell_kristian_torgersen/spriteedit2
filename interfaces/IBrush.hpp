/*
 * IBrush.hpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#ifndef IBRUSH_HPP_
#define IBRUSH_HPP_

namespace kk {

class IBrush {
public:
	virtual int Draw(int x, int y, int color)=0;
	virtual ~IBrush() {
	}
};

} /* namespace kk */

#endif /* IBRUSH_HPP_ */
