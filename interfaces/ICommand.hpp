#ifndef ICOMMAND_HPP_
#define ICOMMAND_HPP_

namespace kk {

class ICommand {
public:
	virtual void Execute(void)=0;
	virtual void Undo(void)=0;
	virtual ~ICommand(){}
};

} /* namespace kk */

#endif /* ICOMMAND_HPP_ */
