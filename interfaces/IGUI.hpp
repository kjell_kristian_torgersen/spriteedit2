/*
 * IGUI.hpp
 *
 *  Created on: Oct 28, 2016
 *      Author: kjell
 */

#ifndef IGUI_HPP_
#define IGUI_HPP_

namespace kk {

class IGUI {
public:
	/** \brief Draw a rectangle*/
	virtual void DrawRect(int x, int y, int w, int h, int color)=0;
	/** \brief Clear screen with given color */
	virtual void Clear(int color)=0;
	/** \brief Display everything drawn since Clear on screen. */
	virtual void Display()=0;
	virtual ~IGUI(){}
};

} /* namespace kk */

#endif /* IGUI_HPP_ */
