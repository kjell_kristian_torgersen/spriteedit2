/*
 * IModel.hpp
 *
 *  Created on: Oct 28, 2016
 *      Author: kjell
 */

#ifndef IMODEL_HPP_
#define IMODEL_HPP_

namespace kk {
enum class Brushes {Pixel, Dither, Random};

class IModel {
public:
	virtual IBitmap * GetSprite()=0;
	virtual IBitmap * GetTiles()=0;
	virtual void SetImage(IBitmap * image)=0;
	virtual void SetTiles(IBitmap * tiles)=0;
	virtual void ExecuteCommand(ICommand * command)=0;
	virtual void Undo(void)=0;
	virtual void ChangeBrush(Brushes newBrush)=0;
	virtual int Draw(int x, int y, int color)=0;
	virtual ~IModel(){}
};

} /* namespace kk */

#endif /* IMODEL_HPP_ */
