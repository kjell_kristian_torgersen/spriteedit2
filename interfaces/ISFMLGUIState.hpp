#ifndef ISFMLGUISTATE_HPP_
#define ISFMLGUISTATE_HPP_

namespace kk {
// Interface for differnt GUI states: Draw state, select tile state ...
class ISFMLGUIState {
public:
	virtual void Draw(void) = 0;
	virtual bool HandleEvents(void) = 0;
	virtual ~ISFMLGUIState() {
	}
};

} /* namespace kk */

#endif /* ISFMLGUISTATE_HPP_ */
