/*
 * TileCmd.hpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#ifndef ITILECMD_HPP_
#define ITILECMD_HPP_

#include "interfaces/ICommand.hpp"

namespace kk {

class ITileCmd: public ICommand {
public:
	virtual void SetSelector(int x, int y)=0;
	virtual ~ITileCmd(){}
};

} /* namespace kk */

#endif /* ITILECMD_HPP_ */
