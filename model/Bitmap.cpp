#include "headers/Bitmap.hpp"
#include <fstream>
namespace kk {

Bitmap::Bitmap(uint16_t width, uint16_t height) :
		width(width),
		height(height) {

	pixels = new uint8_t[width * height];
	columns = new uint8_t*[width];
	for (int x = 0; x < width; x++) {
		columns[x] = &pixels[x * height];
		for (int y = 0; y < height; y++) {
			columns[x][y] = 16;
		}
	}

}

// Return a double pointer so pixel data can be accessed in the form [x][y]
uint8_t** Bitmap::GetImage() const {
	return columns;
}

// Get a single pixel at position x,y
uint8_t Bitmap::GetPixel(int x, int y) const {
	if(x<width&&y<height) {
		return columns[x][y];
	} else {
		return 16;
	}
}

// Set a single pixel to a color. Returns the previous color.
uint8_t Bitmap::SetPixel(int x, int y, uint8_t color) {
	if(x<width&&y<height) {
	uint8_t old = columns[x][y];
	columns[x][y] = color;
	return old;
	} else {
		return 16;
	}
}

// Load a bitmap from a file in our format
void Bitmap::Load(std::string filename) {

	std::ifstream is;
	is.open(filename, std::ios_base::binary);
	if (is.is_open()) {
		is.read((char*) &width, 2);
		is.read((char*) &height, 2);

		delete[] pixels;
		delete[] columns;

		pixels = new uint8_t[width * height];
		columns = new uint8_t*[width];
		for (int x = 0; x < width; x++) {
			columns[x] = &pixels[x * height];
			for (int y = 0; y < height; y++) {
				columns[x][y] = 17;
			}
		}
		//*this = Bitmap(w, h);
		is.read((char*) pixels, (long) width * (long) height);
		is.close();
	}
}

// Save the bitmap a file in our format
void Bitmap::Save(std::string filename) {
	std::ofstream os;
	os.open(filename, std::ios_base::binary);
	os.write((char*) &width, 2);
	os.write((char*) &height, 2);
	os.write((char*) pixels, (long) width * (long) height);
	os.close();
}

void Bitmap::Blit(IBitmap* other, int x0, int y0) {
	int w = other->GetWidth();
	int h = other->GetHeight();

	if ((x0 > width) || (y0 > width))
		return;

	if ((x0 + w > width) || (y0 + h > height))
		return;
	uint8_t ** otherImage = other->GetImage();
	for (int x = 0; x < w; x++) {
		for (int y = 0; y <h; y++) {
			columns[x0 + x][y0 + y] = otherImage[x][y];
		}
	}
}

Bitmap::~Bitmap() {
	delete[] columns;
	delete[] pixels;
}

uint16_t Bitmap::GetHeight() const {
	return height;
}

uint8_t * Bitmap::GetPixels() const {
	return pixels;
}

uint16_t Bitmap::GetWidth() const {
	return width;
}
}
