#include <iostream>
#include <typeinfo>

#include "headers/Model.hpp"
#include "interfaces/IBrush.hpp"
#include "headers/PixelBrush.hpp"
#include "headers/DitherBrush.hpp"
#include "headers/RandBrush.hpp"

namespace kk {
Model::Model() :
				image(new Bitmap(32, 32)),
				tiles(new Bitmap(512, 512)),
				brush(new PixelBrush(image)) {
	Bitmap * bmp = dynamic_cast<Bitmap*>(tiles);
	if (bmp != nullptr)
		bmp->Load("tiles.bin");
}

Model::~Model() {

}

IBitmap * Model::GetSprite() {
	return image;
}

IBitmap * Model::GetTiles() {
	return tiles;
}

void Model::SetImage(IBitmap * image) {
	this->image = image;
}

void Model::SetTiles(IBitmap * tiles) {
	this->tiles = tiles;
}

// Execute and store a command. The command keeps track of necessary changes to undo its action
void Model::ExecuteCommand(ICommand* command) {
	command->Execute();
	commands.push(command); // Save it to a stack
}

void Model::Undo(void) {

	// If there is commands stored on the stack
	if (!commands.empty()) {
		std::cout << "Undo" << std::endl;
		ICommand * cmd = commands.top();
		cmd->Undo(); // Call the most recent command's undo method
		delete cmd; // Delete it
		commands.pop(); // Remove pointer from stack
	}
}

void Model::ChangeBrush(Brushes newBrush) {
	delete (brush);
	switch (newBrush) {
	case Brushes::Pixel:
		brush = new PixelBrush(image);
		break;
	case Brushes::Dither:
		brush = new DitherBrush(image);
		break;
	case Brushes::Random:
		brush = new RandBrush(image);
		break;
	default:
		brush = new PixelBrush(image);
		break;
	}
}

int Model::Draw(int x, int y, int color) {
	return brush->Draw(x, y, color);
}

}
