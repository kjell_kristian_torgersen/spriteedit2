/*
 * DitherBrush.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#include "headers/DitherBrush.hpp"

namespace kk {

DitherBrush::DitherBrush(IBitmap* bitmap) :
		bitmap(bitmap) {
}

int DitherBrush::Draw(int x, int y, int color) {
	if ((x + y) & 1) {
		int oldcolor;
		if (x < 0)
			return 16;
		if (y < 0)
			return 16;
		if (x >= bitmap->GetWidth())
			return 16;
		if (y >= bitmap->GetHeight())
			return 16;
		oldcolor = bitmap->GetImage()[x][y];

		bitmap->GetImage()[x][y] = color;

		return oldcolor;
	} else {
		return 16;
	}
}

DitherBrush::~DitherBrush() {
	// TODO Auto-generated destructor stub
}

} /* namespace kk */
