/*
 * PixelBrush.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#include <headers/PixelBrush.hpp>

namespace kk {



PixelBrush::PixelBrush(IBitmap* bitmap) : bitmap(bitmap) {
}

int PixelBrush::Draw(int x, int y, int color) {
	int oldcolor;
	if(x<0) return 16;
	if(y<0) return 16;
	if(x >= bitmap->GetWidth()) return 16;
	if(y >= bitmap->GetHeight()) return 16;
	oldcolor = bitmap->GetImage()[x][y];
	bitmap->GetImage()[x][y] = color;
	return oldcolor;
}

PixelBrush::~PixelBrush() {
	// TODO Auto-generated destructor stub
}

} /* namespace kk */
