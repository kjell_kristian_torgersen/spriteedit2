/*
 * RandBrush.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#include "headers/RandBrush.hpp"

namespace kk {

RandBrush::RandBrush(IBitmap* bitmap) :
				bitmap(bitmap) {

}

int RandBrush::Draw(int x, int y, int color) {
	/*uint32_t n = (x << 5) + y;
	n = 1664525 * n + 1013904223;
	n = 1664525 * n + 1013904223;
	n = 1664525 * n + 1013904223;*/
	int oldcolor;
	if (x < 0)
		return 16;
	if (y < 0)
		return 16;
	if (x >= bitmap->GetWidth())
		return 16;
	if (y >= bitmap->GetHeight())
		return 16;
	oldcolor = bitmap->GetImage()[x][y];
	if ((rng()) & 0x1) {
		bitmap->GetImage()[x][y] = color;
	}
	return oldcolor;

}

RandBrush::~RandBrush() {
	// TODO Auto-generated destructor stub
}

} /* namespace kk */
