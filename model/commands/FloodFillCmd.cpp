#include "headers/FloodFillCmd.hpp"

namespace kk {

FloodFillCmd::FloodFillCmd(IModel* model, int x, int y, int newColor) :
				model(model),
				undo(model->GetSprite()->GetWidth(), model->GetSprite()->GetHeight()),
				plan(model->GetSprite()->GetWidth(), model->GetSprite()->GetHeight()),
				x(x),
				y(y),
				newColor(newColor),
				image(model->GetSprite()->GetImage()),
				oldColor(0) {

}

// Save state (for undo) of the sprite and perform a flood fill
void FloodFillCmd::Execute(void) {
	uint8_t * pixels = model->GetSprite()->GetPixels();
	uint8_t * undopixels = undo.GetPixels();
	oldColor = image[x][y];
	for (int i = 0; i < undo.GetWidth() * undo.GetHeight(); i++) {
		undopixels[i] = pixels[i];
	}

	// Flood fill is making a plan of which pixels to go through
	FloodFill(x, y);

	for (int x = 0; x < plan.GetWidth(); x++) {
		for (int y = 0; y < plan.GetHeight(); y++) {
			if(plan.GetImage()[x][y] == 0) {
				model->Draw(x,y,newColor);
			}
		}
	}
}

// Perform undo by setting back to previous state
void FloodFillCmd::Undo(void) {
	uint8_t * pixels = model->GetSprite()->GetPixels();
	uint8_t * undopixels = undo.GetPixels();
	for (int i = 0; i < undo.GetWidth() * undo.GetHeight(); i++) {
		pixels[i] = undopixels[i];
	}
}

FloodFillCmd::~FloodFillCmd() {
	// TODO Auto-generated destructor stub
}

// Actually do flood fill. Start at position x,y
void FloodFillCmd::FloodFill(int x, int y) {
	if (x >= undo.GetWidth()) {
		return;
	}
	if (y >= undo.GetHeight()) {
		return;
	}
	if (x < 0) {
		return;
	}

	if (y < 0) {
		return;
	}

	if (plan.GetImage()[x][y] == 0) {
		return;
	}
	if (image[x][y] != oldColor) {
		return;
	}
	plan.SetPixel(x, y, 0);
	//model->Draw(x,y,newColor);
	FloodFill(x + 1, y);
	FloodFill(x - 1, y);
	FloodFill(x, y + 1);
	FloodFill(x, y - 1);
}

} /* namespace kk */
