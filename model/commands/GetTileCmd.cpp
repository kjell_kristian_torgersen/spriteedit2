/*
 * GetTileCmd.cpp
 *
 *  Created on: Oct 27, 2016
 *      Author: kjell
 */

#include "headers/GetTileCmd.hpp"

namespace kk {

GetTileCmd::GetTileCmd(IModel* model) :
		model(model),
		undoSprite(32, 32),
		selectorX(0),
		selectorY(0) {
}

void GetTileCmd::SetSelector(int x, int y) {
	selectorX = x;
	selectorY = y;
}

void GetTileCmd::Execute(void) {
	undoSprite.Blit(model->GetSprite(), 0, 0);
	uint8_t ** tiles = model->GetTiles()->GetImage();
	uint8_t ** sprite = model->GetSprite()->GetImage();

	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 32; y++) {
			sprite[x][y] = tiles[32 * selectorX + x][32 * selectorY + y];
		}
	}
}

void GetTileCmd::Undo(void) {
	model->GetSprite()->Blit(&undoSprite, 0, 0);
}

GetTileCmd::~GetTileCmd() {
	// TODO Auto-generated destructor stub
}

} /* namespace kk */
