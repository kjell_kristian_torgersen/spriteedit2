#include "headers/SetPixelCmd.hpp"

namespace kk {

SetPixelCmd::SetPixelCmd(IModel* model, int x, int y, int newColor) :
				model(model),
				x(x),
				y(y),
				newColor(newColor),
				oldColor(0) {
}
// Just set one pixel to a new color. Store old color in oldColor for undo.
void SetPixelCmd::Execute(void) {
	oldColor = model->Draw(x, y, newColor);
}

// Undo, set back pixel to old color.
void SetPixelCmd::Undo(void) {
	model->GetSprite()->SetPixel(x, y, oldColor);
}

SetPixelCmd::~SetPixelCmd() {
	// TODO Auto-generated destructor stub
}

} /* namespace kk */
