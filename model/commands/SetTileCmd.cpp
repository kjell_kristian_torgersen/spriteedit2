/*
 * SetTileCmd.cpp
 *
 *  Created on: Oct 26, 2016
 *      Author: kjell
 */

#include "headers/SetTileCmd.hpp"

namespace kk {

SetTileCmd::SetTileCmd(IModel * model) :
				model(model),
				undoSprite(32, 32),
				selectorX(0),
				selectorY(0) {

}

void SetTileCmd::Execute(void) {
	uint8_t ** tiles = model->GetTiles()->GetImage();
	uint8_t ** undo = undoSprite.GetImage();
	for (int x = 0; x < 32; x++) {
		for (int y = 0; y < 32; y++) {
			undo[x][y] = tiles[32 * selectorX + x][32 * selectorY + y];
		}
	}
	model->GetTiles()->Blit(model->GetSprite(), 32 * selectorX, 32 * selectorY);
	Bitmap * bmp = dynamic_cast<Bitmap*>(model->GetTiles());
	if (bmp != nullptr) {
		bmp->Save("tiles.bin");
	}
}

void SetTileCmd::Undo(void) {
	model->GetTiles()->Blit(&undoSprite, 32 * selectorX, 32 * selectorY);
	Bitmap * bmp = dynamic_cast<Bitmap*>(model->GetTiles());
	if (bmp != nullptr) {
		bmp->Save("tiles.bin");
	}
}

void SetTileCmd::SetSelector(int x, int y) {
	selectorX = x;
	selectorY = y;
}

SetTileCmd::~SetTileCmd() {
	// TODO Auto-generated destructor stub
}

} /* namespace kk */
