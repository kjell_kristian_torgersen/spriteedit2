#include "headers/SFMLGUI.hpp"
#include "headers/SFMLDrawState.hpp"

// Convert a texture from 8 bits palette to 32 bit RGBA values
void kk::UpdateTexture(sf::Texture* texture, IBitmap* bitmap) {
	static uint32_t palette[17] = { 0xFF000000, 0xFFAA0000, 0xFF00AA00, 0xFFAAAA00, 0xFF0000AA, 0xFFAA00AA, 0xFF0055AA, 0xFFAAAAAA, 0xFF555555, 0xFFFF5555,
			0xFF55FF55, 0xFFFFFF55, 0xFF5555FF, 0xFFFF55FF, 0xFF55FFFF, 0xFFFFFFFF, 0X00000000 };

	int w = bitmap->GetWidth();
	int h = bitmap->GetWidth();
	uint8_t ** image = bitmap->GetImage();
	uint32_t * rgb = new uint32_t[w * h];
	for (int x = 0; x < w; x++) {
		for (int y = 0; y < h; y++) {
			rgb[y * w + x] = palette[image[x][y]];
		}
	}
	texture->update((unsigned char*) rgb);
	delete[] rgb;
}

kk::SFMLGUI::SFMLGUI(kk::IModel * model) :
				model(model),
				window(sf::VideoMode(640, 512), "My window"),
				palette( { { 0x00, 0x00, 0x00, 255 }, { 0x00, 0x00, 0xAA, 255 }, { 0x00, 0xAA, 0x00, 255 }, { 0x00, 0xAA, 0xAA, 255 }, { 0xAA,
						0x00, 0x00, 255 }, { 0xAA, 0x00, 0xAA, 255 }, { 0xAA, 0x55, 0x00, 255 }, { 0xAA, 0xAA, 0xAA, 255 }, { 0x55,
						0x55, 0x55, 255 }, { 0x55, 0x55, 0xFF, 255 }, { 0x55, 0xFF, 0x55, 255 }, { 0x55, 0xFF, 0xFF, 255 }, { 0xFF,
						0x55, 0x55, 255 }, { 0xFF, 0x55, 0xFF, 255 }, { 0xFF, 0xFF, 0x55, 255 }, { 0xFF, 0xFF, 0xFF, 255 }, { 128, 128,
						128, 128 }, }) {
	states.push(new SFMLDrawState(this, 16));
	window.setFramerateLimit(10);
}

void kk::SFMLGUI::Run() {
	// Run the graphical user interface as long as the window is open and there is a view.
	while ((!states.empty()) && window.isOpen()) {
		ISFMLGUIState * state = states.top();
		// Handle events with the current state
		if (state->HandleEvents()) {
			// This state should continue: Redraw window
			state->Draw();
		} else {
			// This state should be deleted
			delete state;
			states.pop();
		}
	}

	// User has closed window. Delete remaining states if any
	while ((!states.empty())) {
		delete states.top();
		states.pop();
	}

}

kk::SFMLGUI::~SFMLGUI() {
}

kk::IModel* kk::SFMLGUI::GetModel() {
	return model;
}

sf::Color* kk::SFMLGUI::GetPalette() {
	return palette;
}

sf::RenderWindow* kk::SFMLGUI::GetWindow() {
	return &window;
}

void kk::SFMLGUI::PushState(ISFMLGUIState* state) {
	states.push(state);
}
