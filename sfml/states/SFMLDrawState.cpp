//#include <headers/SetTileCmd.hpp>
//#include <model/commands/GetTileCmd.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <SFML/System.hpp>
#include "headers/SFMLDrawState.hpp"

#include "headers/Bitmap.hpp"
#include "headers/Model.hpp"
#include "headers/FloodFillCmd.hpp"
#include "headers/SetPixelCmd.hpp"
#include "headers/SFMLPickTileState.hpp"
#include "interfaces/IBrush.hpp"
#include "headers/PixelBrush.hpp"
#include "headers/DitherBrush.hpp"
#include "headers/SetTileCmd.hpp"
#include "headers/GetTileCmd.hpp"

namespace kk {

SFMLDrawState::SFMLDrawState(SFMLGUI * gui, int gridSize) :
				gui(gui),
				drawing(false),
				gridSize(gridSize) {
	sf::Color * colors = gui->GetPalette();

	color[0] = 0;
	color[1] = 15;

	sel[0].setFillColor( { 0, 0, 0, 255 });
	sel[1].setFillColor( { 255, 255, 255, 255 });

	sel[0].setSize( { 24, 24 });
	sel[1].setSize( { 24, 24 });

	for (int i = 0; i < 17; i++) {
		rect[i].setFillColor(colors[i]);
		rect[i].setPosition(2.0, 24 * i + 2.0);
		rect[i].setSize( { 20.0f, 20.0f });
	}
	texture.create(32, 32);
}

void SFMLDrawState::Draw(void) {
	sf::RenderWindow * window = gui->GetWindow();
	// run the program as long as the window is open
	if (window->isOpen()) {

		sf::RectangleShape pixel;
		sf::Sprite sprite;

		pixel.setSize( { (float) (gridSize - 2), (float) (gridSize - 2) });
		sf::Color * colors = gui->GetPalette();
		uint8_t ** pixels = gui->GetModel()->GetSprite()->GetImage();

		// Clear window
		window->clear( { 192, 192, 192 });

		// Draw selectors
		window->draw(sel[0]);
		window->draw(sel[1]);

		// Draw palette
		for (int i = 0; i < 17; i++)
			window->draw(rect[i]);

		// Draw all pixels in sprite
		for (int x = 0; x < 32; x++) {
			for (int y = 0; y < 32; y++) {
				pixel.setPosition(24 + gridSize * x + 1, gridSize * y + 1);
				pixel.setFillColor(colors[pixels[x][y]]);
				window->draw(pixel);
			}
		}

		// draw the sprite 3x3 times at top right of the window
		sprite.setTexture(texture);
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				sprite.setPosition(24 + 512 + 32 * x, 32 * y);
				window->draw(sprite);
			}
		}

		// Put everything to the window
		window->display();
	}

}

bool SFMLDrawState::HandleEvents(void) {
	sf::Event event;
	sf::RenderWindow *window = gui->GetWindow();
	uint8_t ** pixels = gui->GetModel()->GetSprite()->GetImage();

	while (window->pollEvent(event)) {
		// "close requested" event: we close the window
		switch (event.type) {
		case sf::Event::Closed:
			window->close();
			break;
		case sf::Event::MouseButtonPressed:
			drawing = true;
			break;
		case sf::Event::MouseButtonReleased:
			drawing = false;
			break;
		case sf::Event::KeyPressed:
			HandleKeyboard(event);
			break;
		case sf::Event::MouseMoved: {
			int mx = sf::Mouse::getPosition(*window).x;
			int my = sf::Mouse::getPosition(*window).y;
			std::cout << (mx-24)/16 << ". " << (my/16) << std::endl;
		}
			break;
		default:
			break;
		}
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
		int mx = sf::Mouse::getPosition(*window).x;
		int my = sf::Mouse::getPosition(*window).y;
		if (mx < 24) { // kun lag kommando dersom pikselen endres
			color[0] = my / 24;
			if (color[0] >= 16)
				color[0] = 16;
			sel[0].setPosition( { 0.0f, (float) 24.0f * (float) color[0] });
		} else if ((event.mouseButton.x < 24 + 512) && event.mouseButton.y < 512) {
			int x = (mx - 24) / gridSize;
			int y = my / gridSize;
			if (x < 32 && y < 32) {
				if (pixels[x][y] != color[0])
					gui->GetModel()->ExecuteCommand(new SetPixelCmd(gui->GetModel(), x, y, color[0]));
				UpdateTexture(&texture, gui->GetModel()->GetSprite());
			}
		}

	}
	UpdateTexture(&texture, gui->GetModel()->GetSprite());
	return true;
}

bool SFMLDrawState::HandleKeyboard(sf::Event& event) {
	sf::RenderWindow *window = gui->GetWindow();
	int mx = sf::Mouse::getPosition(*window).x;
	int my = sf::Mouse::getPosition(*window).y;
	int x = (mx - 24) / gridSize;
	int y = my / gridSize;
	uint8_t ** pixels = gui->GetModel()->GetSprite()->GetImage();
	switch (event.key.code) {
	case sf::Keyboard::S:
		gui->PushState(new SFMLPickTileState(gui, new SetTileCmd(gui->GetModel()), sf::Color(255, 0, 0, 128)));
		//gui->GetModel()->GetSprite()->Save("bitmap.bin");
		break;
	case sf::Keyboard::L:
		//gui->GetModel()->GetSprite()->Load("bitmap.bin");
		gui->PushState(new SFMLPickTileState(gui, new GetTileCmd(gui->GetModel()), sf::Color(0, 255, 0, 128)));
		break;
	case sf::Keyboard::F:
		if ((x < 32) && (y < 32)) {
			if (pixels[x][y] != color[0]) {
				gui->GetModel()->ExecuteCommand(new FloodFillCmd(gui->GetModel(), x, y, color[0]));
			}
		}
		break;
	case sf::Keyboard::U:
		gui->GetModel()->Undo();
		break;
	case sf::Keyboard::D:
		gui->GetModel()->ChangeBrush(Brushes::Dither);
		break;
	case sf::Keyboard::P:
		gui->GetModel()->ChangeBrush(Brushes::Pixel);
		break;
	case sf::Keyboard::R:
		gui->GetModel()->ChangeBrush(Brushes::Random);
		break;
	default:
		break;
	}
	return true;
}

SFMLDrawState::~SFMLDrawState() {

}

} /* namespace kk */
