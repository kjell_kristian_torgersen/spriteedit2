/*
 * SFMLPickTileState.cpp
 *
 *  Created on: Oct 26, 2016
 *      Author: kjell
 */
#include <SFML/Graphics.hpp>
#include <headers/SFMLPickTileState.hpp>

namespace kk {

SFMLPickTileState::SFMLPickTileState(SFMLGUI * gui, ITileCmd * tilePicked, sf::Color selectorColor) :
		gui(gui),
		tilePicked(tilePicked),
		selectorColor(selectorColor),
		selectorX(0),
		selectorY(0) {
	IBitmap * tilesbmp = gui->GetModel()->GetTiles();
	texture.create(512, 512);
	UpdateTexture(&texture, tilesbmp);
	tiles.setTexture(texture);
}

void SFMLPickTileState::Draw(void) {
	sf::RenderWindow * window = gui->GetWindow();
	if (window->isOpen()) {
		sf::RectangleShape selector;
		selector.setFillColor(selectorColor);
		selector.setPosition(32 * selectorX, 32 * selectorY);
		selector.setSize( { 32.f, 32.f });
		window->clear();
		window->draw(tiles);
		window->draw(selector);
		window->display();
	}
}

bool SFMLPickTileState::HandleEvents(void) {
	sf::Event event;
	sf::RenderWindow *window = gui->GetWindow();
	//uint8_t ** pixels = gui->GetModel()->GetSprite()->GetImage();

	while (window->pollEvent(event)) {
		// "close requested" event: we close the window
		switch (event.type) {
		case sf::Event::Closed:
			window->close();
			return false;
			break;
		case sf::Event::MouseMoved: {
			int mx = sf::Mouse::getPosition(*window).x;
			int my = sf::Mouse::getPosition(*window).y;
			selectorX = mx / 32;
			selectorY = my / 32;
		}
			break;
		case sf::Event::KeyPressed:
			switch (event.key.code) {
			case sf::Keyboard::Escape:
				return false;
			case sf::Keyboard::U:
				gui->GetModel()->Undo();
				UpdateTexture(&texture, gui->GetModel()->GetTiles());
				tiles.setTexture(texture);
				break;
			default:
				break;
			}
			break;
		case sf::Event::MouseButtonPressed: {
			tilePicked->SetSelector(sf::Mouse::getPosition(*window).x / 32, sf::Mouse::getPosition(*window).y / 32);
			gui->GetModel()->ExecuteCommand(tilePicked);
			UpdateTexture(&texture, gui->GetModel()->GetTiles());
			tiles.setTexture(texture);
			return false;
		}
			break;
		default:
			break;
		}

	}
	return true;
}

SFMLPickTileState::~SFMLPickTileState() {
// TODO Auto-generated destructor stub
}

} /* namespace kk */
