#include <iostream>
#include "headers/Model.hpp"
#include "headers/SFMLGUI.hpp"

using namespace std;

int main() {
	kk::Model model;
	kk::SFMLGUI gui(&model);
	gui.Run();
	return 0;
}
